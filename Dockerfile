# syntax = docker/dockerfile:1.3
FROM ubuntu:22.04

RUN apt-get update -y ;\
	apt-get install -y git

# init ssh directory and gather public SSH keys from bitbucket.org
RUN mkdir /root/.ssh/ \
	&& touch /root/.ssh/known_hosts \
	&& ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts

# clone private repository
RUN --mount=type=secret,id=ssh_id,target=/root/.ssh/id_rsa \
	git clone git@bitbucket.org:nomad-digital/nd-obis-mts-with-transfer-overview-notifier.git	

WORKDIR /app
RUN ls -la /app
