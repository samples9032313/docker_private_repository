# About

Sample shows how to handle building private repository from inside a docker file by Jenkins.

When buidl with

```bash
docker build -t app_image .
```

command (without any credentials) `git clone` step fails with

```
Step 4/5 : RUN git clone git@bitbucket.org:nomad-digital/nd-obis-mts-with-transfer-overview-notifier.git
 ---> Running in 21b24943363f
Cloning into 'nd-obis-mts-with-transfer-overview-notifier'...
Host key verification failed.
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.
```

comaplain.

## Sharing private key approach

From `Dockerfile` initialize `.ssh` directory and run `git clone` with `--mount` option

```Dockerfile
# init ssh directory and gather public SSH keys from bitbucket.org
RUN mkdir /root/.ssh/ \
	&& touch /root/.ssh/known_hosts \
	&& ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts

# clone private repository
RUN --mount=type=secret,id=ssh_id,target=/root/.ssh/id_rsa git clone ...
```

from host machine build docker image with `--secret` option this way

```bash
docker build --secret id=ssh_id,src=$HOME/.ssh/private_key -t app_image .
```

where `$HOME/.ssh/private_key` is private key safely shared with docker image.

The build command create secret with `ssh_id` id from a `private_key` file content. On a dockerfile side we mount `ssh_id` secret to `/root/.ssh/id_rsa`.

> **tip**: in case build end with *the --mount option requires BuildKit* complain you need to run with `DOCKER_BUILDKIT=1 docker build ...`
